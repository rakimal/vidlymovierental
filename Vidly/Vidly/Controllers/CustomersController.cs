﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {

        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context= new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customer
        public ActionResult Index()
        {

            //var customers = _context.Customers.Include(c=>c.MembershipType).ToList();

            //var viewModel = new CustomersViewModel() {Customers = customers};


            //return View(viewModel);
            return View();
        }

        public ActionResult CustomerDetails(int id)
        {
            var customer = _context.Customers.Include(c=>c.MembershipType).FirstOrDefault(x => x.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View("CustomerDetails", customer);
        }

        public ActionResult New()
        {
            var membershipTypes = _context.MembershipTypes.ToList();

            var viewModel = new CustomerFormViewModel() {MembershipTypes = membershipTypes};

            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(CustomerFormViewModel formViewModel)
        {
            if (!ModelState.IsValid)
            {

                //var errors = ModelState.Select(x => x.Value.Errors)
                //    .Where(y => y.Count > 0)
                //    .ToList();

                var viewModel = new CustomerFormViewModel()
                {
                    Customer = formViewModel.Customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("CustomerForm", viewModel);
            }

            if (!formViewModel.Customer.Id.HasValue)
                _context.Customers.Add(formViewModel.Customer);
            else
            {
                var customerInDb = _context.Customers.FirstOrDefault(c => c.Id == formViewModel.Customer.Id);

                customerInDb.Name = formViewModel.Customer.Name;
                customerInDb.BirthDate = formViewModel.Customer.BirthDate;
                customerInDb.IsSubscribedToNewsLetter = formViewModel.Customer.IsSubscribedToNewsLetter;
                customerInDb.MembershipTypeId = formViewModel.Customer.MembershipTypeId;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }

        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.FirstOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = new CustomerFormViewModel()
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };


            return View("CustomerForm", viewModel);
        }
    }
}