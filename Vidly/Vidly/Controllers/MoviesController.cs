﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Security;

namespace Vidly.Controllers
{

    //Test comment
    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context= new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        public ActionResult Index()
        {


            //var movies = _context.Movies.Include(m => m.Genre).ToList();

            //var viewModel = new MoviesViewModel() {Movies = movies};

            //return View(viewModel);

            if (User.IsInRole(RoleName.CanManageMovies))
                return View("List");

            return View("ReadOnlyList");
        }

        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult MovieDetails(int id)
        {
            var movie = _context.Movies.Include(m=>m.Genre).FirstOrDefault(x => x.Id == id);

            if (movie == null)
                return HttpNotFound();

            return View(movie);
        }

        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult New()
        {
            var genres = _context.Genres.ToList();

            var viewModel = new MoviesFormViewModel()
            {
                //Movie = new Movies(),
                Genres = genres
            };

            return View("MovieForm", viewModel);
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Save(MoviesFormViewModel formViewModel)
        {
            if (formViewModel.Movie != null)
            {
                formViewModel.Movie.DateAdded = DateTime.Now;

                var genre = _context.Genres.FirstOrDefault(g => g.Id == formViewModel.Movie.GenreId);

                formViewModel.Movie.Genre = genre;
            }
           

            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                var viewModel = new MoviesFormViewModel()
                {
                    Movie = formViewModel.Movie,
                    Genres = _context.Genres.ToList()
                };

                return View("MovieForm", viewModel);
            }

            if (formViewModel.Movie != null && (!formViewModel.Movie.Id.HasValue || formViewModel.Movie.Id == 0))
            {
                _context.Movies.Add(formViewModel.Movie);
            }
            else
            {
                var movieInDb = _context.Movies.FirstOrDefault(m => m.Id == formViewModel.Movie.Id);

                var genreEdit = _context.Genres.FirstOrDefault(g => g.Id == formViewModel.Movie.GenreId);

                movieInDb.Name = formViewModel.Movie.Name;
                movieInDb.ReleaseDate = formViewModel.Movie.ReleaseDate;
                movieInDb.GenreId = formViewModel.Movie.GenreId;
                movieInDb.NumberInStock = formViewModel.Movie.NumberInStock;
                movieInDb.Genre = genreEdit;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Movies");
        }

        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);

            if (movie == null)
                return HttpNotFound();

            

            var viewModel= new MoviesFormViewModel()
            {
                Movie = movie,
                Genres = _context.Genres.ToList()
            };

            return View("MovieForm", viewModel);
        }

      

     
    }
}