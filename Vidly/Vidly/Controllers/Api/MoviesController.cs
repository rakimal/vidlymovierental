﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context= new ApplicationDbContext();
        }

        //Get /api/Movies
        //[HttpGet]
        //public IHttpActionResult GetAllMovies()
        //{
        //    return Ok(_context.Movies
        //        .Include(m => m.Genre)
        //        .ToList()
        //        .Select(Mapper.Map<Movies, MoviesDto>));


        //}

        //Get /api/Movies
        [HttpGet]
        public IHttpActionResult GetMovies(string query=null)
        {
            var moviesQuery= _context.Movies.Include(m=>m.Genre).Where(m=>m.NumberAvailable>0);

            if (!string.IsNullOrWhiteSpace(query))
                moviesQuery = moviesQuery.Where(m => m.Name.Contains(query));


            return Ok(moviesQuery.ToList().Select(Mapper.Map<Movies, MoviesDto>));
           
        }


        //GET /api/Movies/1
        [System.Web.Http.Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult GetMovie(int id)
        {
            //Even the below commented approach can be used
            //if (!User.IsInRole(RoleName.CanManageMovies))
            //    return Unauthorized();

            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);

            if (movie == null)
                return NotFound();

            var movieDto = Mapper.Map<Movies, MoviesDto>(movie);

            return Ok(movieDto);
        }

        //POST /api/Movies
        [HttpPost]
        [System.Web.Http.Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult CreateMovie(MoviesDto movieDto)
        {

            if (!ModelState.IsValid)
                return BadRequest();

            var movie = Mapper.Map<MoviesDto, Movies>(movieDto);

            _context.Movies.Add(movie);

            _context.SaveChanges();

            movieDto.Id = movie.Id;

            return Created(new Uri(Request.RequestUri + "/" + movieDto.Id), movieDto);
        }

        //PUT /api/Movie/1
        [HttpPut]
        [System.Web.Http.Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult UpdateMovie(int id, MoviesDto moviesDto)
        {


            if (!ModelState.IsValid)
                return BadRequest();

            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);

            if (movie == null)
                return NotFound();

            Mapper.Map(moviesDto, movie);

            _context.SaveChanges();

            return Ok(moviesDto);
        }


        //Delete /api/movies/1

        [HttpDelete]
        [System.Web.Http.Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult DeleteMovie(int id)
        {
            var movieinDb = _context.Movies.FirstOrDefault(m => m.Id == id);

            if (movieinDb == null)
                return NotFound();

            _context.Movies.Remove(movieinDb);

            _context.SaveChanges();

            return Ok();
        }

    }
}
