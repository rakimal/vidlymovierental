﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class MoviesRentalController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesRentalController()
        {
            _context=new ApplicationDbContext();
        }


        [HttpPost]
        public IHttpActionResult CreateNewMovieRental(MovieRentalDto movieRentalDto)
        {
            var custumer = _context.Customers.FirstOrDefault(c => c.Id == movieRentalDto.CustomerId);

            foreach (int id in movieRentalDto.MovieIds)
            {
                var movie = _context.Movies.FirstOrDefault(m => m.Id == id);

                if (movie.NumberAvailable == 0)
                    return BadRequest("Movie is not Available");

                movie.NumberAvailable--;

                var rental = new Rental()
                {
                    Customer = custumer,
                    Movie = movie,
                    DateRented = DateTime.Now,
                    DateReturned = null
                };

                _context.Rentals.Add(rental);
               
            }

            _context.SaveChanges();
            return Ok();
        }


    }
}
