﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context= new ApplicationDbContext();
        }
        
        //GET api/Customers
        public IHttpActionResult GetCustomers(string query=null)
        {

           var customersQuery=_context.Customers
                              .Include(c=>c.MembershipType);

            if (!string.IsNullOrWhiteSpace(query))
                customersQuery = customersQuery.Where(c => c.Name.Contains(query));


            return Ok(customersQuery.ToList().Select(Mapper.Map<Customer, CustomerDto>));

        }

        //Get api/Customers/1
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _context.Customers.FirstOrDefault(c => c.Id == id);

            if (customer==null)
                throw  new HttpResponseException(HttpStatusCode.NotFound);

            var customerDto = Mapper.Map<Customer, CustomerDto>(customer);


            return Ok(customerDto);
        }

        //Post api/Customers
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                BadRequest();

            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);

            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.Id = customer.Id.Value;

            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);

        }


        //Put api/Customers/1
        [HttpPut]
        public void UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);


            var customerinDB = _context.Customers.FirstOrDefault(c => c.Id == id);

            if (customerinDB == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            Mapper.Map(customerDto, customerinDB);

            _context.SaveChanges();
        }


        //Delete api/Customers/1
        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customer = _context.Customers.FirstOrDefault(c => c.Id == id);

            if (customer == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            _context.Customers.Remove(customer);

            _context.SaveChanges();

            return Ok();
        }
    }
}
