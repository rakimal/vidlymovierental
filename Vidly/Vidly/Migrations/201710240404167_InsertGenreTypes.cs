namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertGenreTypes : DbMigration
    {
        public override void Up()
        {
            Sql("Insert into Genres(Name) Values('Comedy')");
            Sql("Insert into Genres(Name) Values('Action')");
            Sql("Insert into Genres(Name) Values('Romance')");
            Sql("Insert into Genres(Name) Values('Horror')");
            Sql("Insert into Genres(Name) Values('SciFi')");


        }
        
        public override void Down()
        {
        }
    }
}
