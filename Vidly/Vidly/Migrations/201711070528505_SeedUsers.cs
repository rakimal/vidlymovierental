namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'118f0a55-06fb-4aae-b5ad-fd525146fe6d', N'guest@vidly.com', 0, N'ACMs9PK+8sJ9T5hBbQJPzAlSMOmYZZHmBTBTph+H3NW9hf4sCJ7Y97asF9V92T2SAg==', N'4f007a14-04f7-4d3b-830f-5c9ce65739e2', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fc6a92fd-4e4c-42ba-a827-d398cfc78529', N'admin@vidly.com', 0, N'AL1t3I4y5ftlI9v7pKowB1QgZsK5SdaD3n9H50/odq1SEyVCHBbx6FQbcIzuxq0v4A==', N'34c17c17-c208-4a50-9946-ca9b4bd6fe18', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')

                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'165f85ed-35e4-4271-9c61-00b6983c8f27', N'CanManageMovies')
                
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fc6a92fd-4e4c-42ba-a827-d398cfc78529', N'165f85ed-35e4-4271-9c61-00b6983c8f27')
                ");
        }
        
        public override void Down()
        {
        }
    }
}
