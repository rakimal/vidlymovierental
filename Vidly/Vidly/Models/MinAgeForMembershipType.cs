﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class MinAgeForMembershipType:ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            if(customer.MembershipTypeId== (byte)MemberhsipType.Unknown || customer.MembershipTypeId== (byte)MemberhsipType.PayAsYouGo)
                return ValidationResult.Success;

            if (customer.BirthDate==null)
                return new ValidationResult("Birthdate is required.");

            int age = DateTime.Now.Year - customer.BirthDate.Value.Year;

            return age>=18
                ? ValidationResult.Success 
                : new ValidationResult("The member must be 18 years or older to go on with this membership.");
        }
    }
}